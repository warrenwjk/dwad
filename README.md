# dwad - Datalog-based WebAssembly Decompiler

## Requirements

* [Cargo](https://github.com/rust-lang/cargo/)
* [Soufflé](https://souffle-lang.github.io/) compiled with `SOUFFLE_DOMAIN_64BIT`

## Setup

Run the following commands:

```sh
g++ -c -fPIC -fopenmp -I<path-to-souffle-repository>/include -o functors.o src/datalog/functors.cpp
g++ -shared -o libfunctors.so functors.o
cargo build --release
```

Then add `target/release` to `$PATH`.

## Usage

```sh
dwad 
Datalog-based WebAssembly decompiler

USAGE:
    dwad [OPTIONS] <INPUT_FILE>

ARGS:
    <INPUT_FILE>    WebAssembly file to decompile

OPTIONS:
    -h, --help                     Print help information
    -n, --num-cores <NUM_CORES>    Number of CPU cores to use [default: 1]
    -o, --out-file <OUT_FILE>      File to write the decompiled program to [default: out.c]
```

```sh
code-gen 
C code generator

USAGE:
    code-gen [OPTIONS] <NUM_PROGRAMS> <COMPLEXITY>

ARGS:
    <NUM_PROGRAMS>    Number of programs to generate
    <COMPLEXITY>      Complexity of generated programs [possible values: low, low2, normal,
                      normal2, high]

OPTIONS:
    -h, --help                 Print help information
    -o, --out-dir <OUT_DIR>    Directory to write generated programs to [default: gen]
```
