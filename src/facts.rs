use crate::OUT_DIR;
use id_arena::Id;
use std::{cell::RefCell, collections::HashMap, fs, io, path::Path};
use walrus::{
    ir::{
        BinaryOp, Binop, ExtendedLoad, Instr, InstrSeq, LoadKind, StoreKind, UnaryOp, Unop, Value,
    },
    ActiveDataLocation, DataKind, ElementKind, FunctionKind, GlobalKind, InitExpr, LocalFunction,
    Module, ValType,
};

macro_rules! fact {
    ($($ids:expr),+) => {
        {
            let mut res = String::new();
            $(
                res.push_str(&$ids.to_string());
                res.push('\u{0009}');
            )+
            res.push('\n');
            res
        }
    };
}

#[derive(Debug)]
pub(crate) struct Facts<'a> {
    module: Module,
    facts: RefCell<HashMap<&'a str, String>>,
}

impl<'a> Facts<'a> {
    pub(crate) fn generate(input_file: impl AsRef<Path>) -> walrus::Result<()> {
        let mut facts = Facts {
            module: Module::from_file(input_file)?,
            facts: RefCell::new(HashMap::new()),
        };
        facts.generate_type_facts()?;
        facts.generate_function_facts()?;
        facts.generate_table_facts()?;
        facts.generate_memory_facts()?;
        facts.generate_global_facts()?;
        facts.generate_elem_facts()?;
        facts.generate_data_facts()?;
        facts.emit_files()?;
        Ok(())
    }

    fn emit_files(&self) -> io::Result<()> {
        for (file, facts) in self
            .facts
            .borrow()
            .iter()
            .filter(|(_, facts)| !facts.is_empty())
        {
            fs::write(format!("{OUT_DIR}/{}.facts", file), facts.as_bytes())?;
        }
        Ok(())
    }

    fn append_fact(&self, file: &'a str, fact: &str) {
        self.facts
            .borrow_mut()
            .entry(file)
            .and_modify(|facts| facts.push_str(fact))
            .or_insert_with(|| fact.to_string());
    }

    fn generate_type_facts(&mut self) -> io::Result<()> {
        for (tpe_idx, tpe) in self.module.types.iter().enumerate() {
            for (param_idx, &param) in tpe.params().iter().enumerate() {
                let param_tpe = type_to_datalog(param);
                self.append_fact("TypeParams", &fact!(tpe_idx, param_idx, param_tpe));
            }
            for (result_idx, &result) in tpe.results().iter().enumerate() {
                let result_tpe = type_to_datalog(result);
                self.append_fact("TypeResults", &fact!(tpe_idx, result_idx, result_tpe));
            }
        }
        Ok(())
    }

    fn generate_function_facts(&mut self) -> io::Result<()> {
        for (function_idx, function) in self.module.functions().enumerate() {
            let function_tpe = function.ty().index();
            let function_name_default = format!("f{function_idx}");
            let function_name = function.name.as_ref().unwrap_or(&function_name_default);
            if function_name.starts_with("legalstub") {
                continue;
            }
            self.append_fact(
                "Function",
                &fact!(function_idx, function_tpe, function_name),
            );
            if let FunctionKind::Local(function) = &function.kind {
                for (local_idx, &local) in function.args.iter().chain(&function.locals).enumerate()
                {
                    let local_id = local.index();
                    let local_tpe = type_to_datalog(self.module.locals.get(local).ty());
                    let local_name_default = format!("local{local_idx}");
                    let local_name = self
                        .module
                        .locals
                        .get(local)
                        .name
                        .as_ref()
                        .unwrap_or(&local_name_default);
                    self.append_fact(
                        "FunctionLocals",
                        &fact!(function_idx, local_idx, local_id, local_tpe, local_name),
                    );
                }
                let block = function.entry_block();
                self.generate_instruction_facts(function_idx, function, block)?;
                let pos = format!("[[{},{}],{}]", function_idx, 2147483647, 2147483647);
                self.append_fact("PosExists", &fact!(pos));
            }
        }
        Ok(())
    }

    fn generate_instruction_facts(
        &self,
        function_idx: usize,
        function: &LocalFunction,
        block: Id<InstrSeq>,
    ) -> io::Result<()> {
        let block_idx = block.index();
        for (instr_idx, (instr, _)) in function.block(block).iter().enumerate() {
            let pos = format!("[[{},{}],{}]", function_idx, block_idx, instr_idx);
            if !matches!(instr, Instr::Unreachable(_)) {
                self.append_fact("PosExists", &fact!(pos));
            }
            match instr {
                Instr::Block(instr) => {
                    let block = instr.seq;
                    let block_id = format!("[{},{}]", function_idx, block.index());
                    self.append_fact("BlockInstr", &fact!(pos, block_id));
                    self.generate_instruction_facts(function_idx, function, block)?;
                }
                Instr::Loop(instr) => {
                    let block = instr.seq;
                    let block_id = format!("[{},{}]", function_idx, block.index());
                    self.append_fact("Loop", &fact!(pos, block_id));
                    self.generate_instruction_facts(function_idx, function, block)?;
                }
                Instr::Call(instr) => {
                    let function = instr.func.index();
                    self.append_fact("Call", &fact!(pos, function));
                }
                Instr::CallIndirect(instr) => {
                    let table = instr.table.index();
                    let tpe = instr.ty.index();
                    self.append_fact("CallIndirect", &fact!(pos, table, tpe));
                }
                Instr::LocalGet(instr) => {
                    let local = instr.local.index();
                    self.append_fact("LocalGet", &fact!(pos, local));
                }
                Instr::LocalSet(instr) => {
                    let local = instr.local.index();
                    self.append_fact("LocalSet", &fact!(pos, local));
                }
                Instr::LocalTee(instr) => {
                    let local = instr.local.index();
                    self.append_fact("LocalTee", &fact!(pos, local));
                }
                Instr::GlobalGet(instr) => {
                    let global = instr.global.index();
                    self.append_fact("GlobalGet", &fact!(pos, global));
                }
                Instr::GlobalSet(instr) => {
                    let global = instr.global.index();
                    self.append_fact("GlobalSet", &fact!(pos, global));
                }
                Instr::Const(instr) => match instr.value {
                    Value::I32(value) => self.append_fact("IConst", &fact!(pos, "int", value)),
                    Value::I64(value) => {
                        self.append_fact("IConst", &fact!(pos, "long long", value))
                    }
                    Value::F32(value) => self.append_fact("FConst", &fact!(pos, "float", value)),
                    Value::F64(value) => self.append_fact("FConst", &fact!(pos, "double", value)),
                    Value::V128(_) => unimplemented!(),
                },
                Instr::Binop(instr) => self.generate_binop_fact(&pos, instr),
                Instr::Unop(instr) => self.generate_unop_fact(&pos, instr),
                Instr::Select(instr) => match instr.ty {
                    Some(tpe) => {
                        let tpe = type_to_datalog(tpe);
                        self.append_fact("TypedSelect", &fact!(pos, tpe));
                    }
                    None => self.append_fact("Select", &fact!(pos)),
                },
                Instr::Unreachable(_) => {}
                Instr::Br(instr) => {
                    let block = format!("[{},{}]", function_idx, instr.block.index());
                    self.append_fact("Br", &fact!(pos, block));
                }
                Instr::BrIf(instr) => {
                    let block = format!("[{},{}]", function_idx, instr.block.index());
                    self.append_fact("BrIf", &fact!(pos, block));
                }
                Instr::IfElse(instr) => {
                    let then_block = instr.consequent;
                    let then_block_id = format!("[{},{}]", function_idx, then_block.index());
                    let else_block = instr.alternative;
                    let else_block_id = format!("[{},{}]", function_idx, else_block.index());
                    self.append_fact("If", &fact!(pos, then_block_id, else_block_id));
                    self.generate_instruction_facts(function_idx, function, then_block)?;
                    self.generate_instruction_facts(function_idx, function, else_block)?;
                }
                Instr::BrTable(instr) => {
                    let block = format!("[{},{}]", function_idx, instr.default.index());
                    self.append_fact("BrTable", &fact!(pos, block));
                    for (idx, block) in instr.blocks.iter().enumerate() {
                        let block = format!("[{},{}]", function_idx, block.index());
                        self.append_fact("BrTableLabels", &fact!(pos, idx, block));
                    }
                }
                Instr::Drop(_) => self.append_fact("Drop", &fact!(pos)),
                Instr::Return(_) => self.append_fact("Return", &fact!(pos)),
                Instr::MemorySize(_) => self.append_fact("MemorySize", &fact!(pos)),
                Instr::MemoryGrow(_) => self.append_fact("MemoryGrow", &fact!(pos)),
                Instr::MemoryInit(instr) => {
                    let data = instr.data.index();
                    self.append_fact("MemoryInit", &fact!(pos, data));
                }
                Instr::DataDrop(instr) => {
                    let data = instr.data.index();
                    self.append_fact("MemoryInit", &fact!(pos, data));
                }
                Instr::MemoryCopy(_) => self.append_fact("MemoryCopy", &fact!(pos)),
                Instr::MemoryFill(_) => self.append_fact("MemoryFill", &fact!(pos)),
                Instr::Load(instr) => {
                    let offset = instr.arg.offset;
                    let align = instr.arg.align;
                    match instr.kind {
                        LoadKind::I32 { atomic: false } => {
                            self.append_fact("Load", &fact!(pos, "int", offset, align))
                        }
                        LoadKind::I32 { atomic: true } => unimplemented!(),
                        LoadKind::I64 { atomic: false } => {
                            self.append_fact("Load", &fact!(pos, "long long", offset, align))
                        }
                        LoadKind::I64 { atomic: true } => unimplemented!(),
                        LoadKind::F32 => {
                            self.append_fact("Load", &fact!(pos, "float", offset, align))
                        }
                        LoadKind::F64 => {
                            self.append_fact("Load", &fact!(pos, "double", offset, align))
                        }
                        LoadKind::V128 => unimplemented!(),
                        LoadKind::I32_8 { kind } => {
                            let sx = load_kind_to_datalog(kind);
                            self.append_fact("Load8", &fact!(pos, "int", sx, offset, align));
                        }
                        LoadKind::I32_16 { kind } => {
                            let sx = load_kind_to_datalog(kind);
                            self.append_fact("Load16", &fact!(pos, "int", sx, offset, align));
                        }
                        LoadKind::I64_8 { kind } => {
                            let sx = load_kind_to_datalog(kind);
                            self.append_fact("Load8", &fact!(pos, "long long", sx, offset, align));
                        }
                        LoadKind::I64_16 { kind } => {
                            let sx = load_kind_to_datalog(kind);
                            self.append_fact("Load16", &fact!(pos, "long long", sx, offset, align));
                        }
                        LoadKind::I64_32 { kind } => {
                            let sx = load_kind_to_datalog(kind);
                            self.append_fact("Load32", &fact!(pos, "long long", sx, offset, align));
                        }
                    }
                }
                Instr::Store(instr) => {
                    let offset = instr.arg.offset;
                    let align = instr.arg.align;
                    match instr.kind {
                        StoreKind::I32 { atomic: false } => {
                            self.append_fact("Store", &fact!(pos, "int", offset, align))
                        }
                        StoreKind::I32 { atomic: true } => unimplemented!(),
                        StoreKind::I64 { atomic: false } => {
                            self.append_fact("Store", &fact!(pos, "long long", offset, align))
                        }
                        StoreKind::I64 { atomic: true } => unimplemented!(),
                        StoreKind::F32 => {
                            self.append_fact("Store", &fact!(pos, "float", offset, align))
                        }
                        StoreKind::F64 => {
                            self.append_fact("Store", &fact!(pos, "double", offset, align))
                        }
                        StoreKind::V128 => unimplemented!(),
                        StoreKind::I32_8 { atomic: false } => {
                            self.append_fact("Store8", &fact!(pos, "int", offset, align))
                        }
                        StoreKind::I32_8 { atomic: true } => unimplemented!(),
                        StoreKind::I32_16 { atomic: false } => {
                            self.append_fact("Store16", &fact!(pos, "int", offset, align))
                        }
                        StoreKind::I32_16 { atomic: true } => unimplemented!(),
                        StoreKind::I64_8 { atomic: false } => {
                            self.append_fact("Store8", &fact!(pos, "long long", offset, align))
                        }
                        StoreKind::I64_8 { atomic: true } => unimplemented!(),
                        StoreKind::I64_16 { atomic: false } => {
                            self.append_fact("Store16", &fact!(pos, "long long", offset, align))
                        }
                        StoreKind::I64_16 { atomic: true } => unimplemented!(),
                        StoreKind::I64_32 { atomic: false } => {
                            self.append_fact("Store32", &fact!(pos, "long long", offset, align))
                        }
                        StoreKind::I64_32 { atomic: true } => unimplemented!(),
                    }
                }
                Instr::AtomicRmw(_)
                | Instr::Cmpxchg(_)
                | Instr::AtomicNotify(_)
                | Instr::AtomicWait(_)
                | Instr::AtomicFence(_) => unimplemented!(),
                Instr::TableGet(instr) => {
                    let table = instr.table.index();
                    self.append_fact("TableGet", &fact!(pos, table));
                }
                Instr::TableSet(instr) => {
                    let table = instr.table.index();
                    self.append_fact("TableSet", &fact!(pos, table));
                }
                Instr::TableGrow(instr) => {
                    let table = instr.table.index();
                    self.append_fact("TableGrow", &fact!(pos, table));
                }
                Instr::TableSize(instr) => {
                    let table = instr.table.index();
                    self.append_fact("TableSize", &fact!(pos, table));
                }
                Instr::TableFill(instr) => {
                    let table = instr.table.index();
                    self.append_fact("TableFill", &fact!(pos, table));
                }
                Instr::RefNull(instr) => {
                    let tpe = type_to_datalog(instr.ty);
                    self.append_fact("Null", &fact!(pos, tpe));
                }
                Instr::RefIsNull(_) => self.append_fact("IsNull", &fact!(pos)),
                Instr::RefFunc(instr) => {
                    let function = instr.func.index();
                    self.append_fact("Func", &fact!(pos, function));
                }
                Instr::V128Bitselect(_)
                | Instr::I8x16Swizzle(_)
                | Instr::I8x16Shuffle(_)
                | Instr::LoadSimd(_) => unimplemented!(),
                Instr::TableInit(instr) => {
                    let table = instr.table.index();
                    let elem = instr.elem.index();
                    self.append_fact("TableInit", &fact!(pos, table, elem));
                }
                Instr::ElemDrop(instr) => {
                    let elem = instr.elem.index();
                    self.append_fact("ElemDrop", &fact!(pos, elem));
                }
                Instr::TableCopy(instr) => {
                    let dest = instr.dst.index();
                    let src = instr.src.index();
                    self.append_fact("TableCopy", &fact!(pos, dest, src));
                }
            }
        }
        Ok(())
    }

    fn generate_binop_fact(&self, pos: &str, instr: &Binop) {
        match instr.op {
            BinaryOp::I32Eq => self.append_fact("Eq", &fact!(pos)),
            BinaryOp::I32Ne => self.append_fact("Ne", &fact!(pos)),
            BinaryOp::I32LtS => self.append_fact("ILt", &fact!(pos, "$s")),
            BinaryOp::I32LtU => self.append_fact("ILt", &fact!(pos, "$u")),
            BinaryOp::I32GtS => self.append_fact("IGt", &fact!(pos, "$s")),
            BinaryOp::I32GtU => self.append_fact("IGt", &fact!(pos, "$u")),
            BinaryOp::I32LeS => self.append_fact("ILe", &fact!(pos, "$s")),
            BinaryOp::I32LeU => self.append_fact("ILe", &fact!(pos, "$u")),
            BinaryOp::I32GeS => self.append_fact("IGe", &fact!(pos, "$s")),
            BinaryOp::I32GeU => self.append_fact("IGe", &fact!(pos, "$u")),
            BinaryOp::I64Eq => self.append_fact("Eq", &fact!(pos)),
            BinaryOp::I64Ne => self.append_fact("Ne", &fact!(pos)),
            BinaryOp::I64LtS => self.append_fact("ILt", &fact!(pos, "$s")),
            BinaryOp::I64LtU => self.append_fact("ILt", &fact!(pos, "$u")),
            BinaryOp::I64GtS => self.append_fact("IGt", &fact!(pos, "$s")),
            BinaryOp::I64GtU => self.append_fact("IGt", &fact!(pos, "$u")),
            BinaryOp::I64LeS => self.append_fact("ILe", &fact!(pos, "$s")),
            BinaryOp::I64LeU => self.append_fact("ILe", &fact!(pos, "$u")),
            BinaryOp::I64GeS => self.append_fact("IGe", &fact!(pos, "$s")),
            BinaryOp::I64GeU => self.append_fact("IGe", &fact!(pos, "$u")),
            BinaryOp::F32Eq => self.append_fact("Eq", &fact!(pos)),
            BinaryOp::F32Ne => self.append_fact("Ne", &fact!(pos)),
            BinaryOp::F32Lt => self.append_fact("FLt", &fact!(pos)),
            BinaryOp::F32Gt => self.append_fact("FGt", &fact!(pos)),
            BinaryOp::F32Le => self.append_fact("FLe", &fact!(pos)),
            BinaryOp::F32Ge => self.append_fact("FGe", &fact!(pos)),
            BinaryOp::F64Eq => self.append_fact("Eq", &fact!(pos)),
            BinaryOp::F64Ne => self.append_fact("Ne", &fact!(pos)),
            BinaryOp::F64Lt => self.append_fact("FLt", &fact!(pos)),
            BinaryOp::F64Gt => self.append_fact("FGt", &fact!(pos)),
            BinaryOp::F64Le => self.append_fact("FLe", &fact!(pos)),
            BinaryOp::F64Ge => self.append_fact("FGe", &fact!(pos)),
            BinaryOp::I32Add => self.append_fact("Add", &fact!(pos, "int")),
            BinaryOp::I32Sub => self.append_fact("Sub", &fact!(pos, "int")),
            BinaryOp::I32Mul => self.append_fact("Mul", &fact!(pos, "int")),
            BinaryOp::I32DivS => self.append_fact("IDiv", &fact!(pos, "int", "$s")),
            BinaryOp::I32DivU => self.append_fact("IDiv", &fact!(pos, "int", "$u")),
            BinaryOp::I32RemS => self.append_fact("Rem", &fact!(pos, "int", "$s")),
            BinaryOp::I32RemU => self.append_fact("Rem", &fact!(pos, "int", "$u")),
            BinaryOp::I32And => self.append_fact("And", &fact!(pos, "int")),
            BinaryOp::I32Or => self.append_fact("Or", &fact!(pos, "int")),
            BinaryOp::I32Xor => self.append_fact("Xor", &fact!(pos, "int")),
            BinaryOp::I32Shl => self.append_fact("Shl", &fact!(pos, "int")),
            BinaryOp::I32ShrS => self.append_fact("Shr", &fact!(pos, "int", "$s")),
            BinaryOp::I32ShrU => self.append_fact("Shr", &fact!(pos, "int", "$u")),
            BinaryOp::I32Rotl => self.append_fact("Rotl", &fact!(pos, "int")),
            BinaryOp::I32Rotr => self.append_fact("Rotr", &fact!(pos, "int")),
            BinaryOp::I64Add => self.append_fact("Add", &fact!(pos, "long long")),
            BinaryOp::I64Sub => self.append_fact("Sub", &fact!(pos, "long long")),
            BinaryOp::I64Mul => self.append_fact("Mul", &fact!(pos, "long long")),
            BinaryOp::I64DivS => self.append_fact("IDiv", &fact!(pos, "long long", "$s")),
            BinaryOp::I64DivU => self.append_fact("IDiv", &fact!(pos, "long long", "$u")),
            BinaryOp::I64RemS => self.append_fact("Rem", &fact!(pos, "long long", "$s")),
            BinaryOp::I64RemU => self.append_fact("Rem", &fact!(pos, "long long", "$u")),
            BinaryOp::I64And => self.append_fact("And", &fact!(pos, "long long")),
            BinaryOp::I64Or => self.append_fact("Or", &fact!(pos, "long long")),
            BinaryOp::I64Xor => self.append_fact("Xor", &fact!(pos, "long long")),
            BinaryOp::I64Shl => self.append_fact("Shl", &fact!(pos, "long long")),
            BinaryOp::I64ShrS => self.append_fact("Shr", &fact!(pos, "long long", "$s")),
            BinaryOp::I64ShrU => self.append_fact("Shr", &fact!(pos, "long long", "$u")),
            BinaryOp::I64Rotl => self.append_fact("Rotl", &fact!(pos, "long long")),
            BinaryOp::I64Rotr => self.append_fact("Rotr", &fact!(pos, "long long")),
            BinaryOp::F32Add => self.append_fact("Add", &fact!(pos, "float")),
            BinaryOp::F32Sub => self.append_fact("Sub", &fact!(pos, "float")),
            BinaryOp::F32Mul => self.append_fact("Mul", &fact!(pos, "float")),
            BinaryOp::F32Div => self.append_fact("FDiv", &fact!(pos, "float")),
            BinaryOp::F32Min => self.append_fact("Min", &fact!(pos, "float")),
            BinaryOp::F32Max => self.append_fact("Max", &fact!(pos, "float")),
            BinaryOp::F32Copysign => self.append_fact("Copysign", &fact!(pos, "float")),
            BinaryOp::F64Add => self.append_fact("Add", &fact!(pos, "double")),
            BinaryOp::F64Sub => self.append_fact("Sub", &fact!(pos, "double")),
            BinaryOp::F64Mul => self.append_fact("Mul", &fact!(pos, "double")),
            BinaryOp::F64Div => self.append_fact("FDiv", &fact!(pos, "double")),
            BinaryOp::F64Min => self.append_fact("Min", &fact!(pos, "double")),
            BinaryOp::F64Max => self.append_fact("Max", &fact!(pos, "double")),
            BinaryOp::F64Copysign => self.append_fact("Copysign", &fact!(pos, "double")),
            BinaryOp::I8x16ReplaceLane { idx: _ }
            | BinaryOp::I16x8ReplaceLane { idx: _ }
            | BinaryOp::I32x4ReplaceLane { idx: _ }
            | BinaryOp::I64x2ReplaceLane { idx: _ }
            | BinaryOp::F32x4ReplaceLane { idx: _ }
            | BinaryOp::F64x2ReplaceLane { idx: _ }
            | BinaryOp::I8x16Eq
            | BinaryOp::I8x16Ne
            | BinaryOp::I8x16LtS
            | BinaryOp::I8x16LtU
            | BinaryOp::I8x16GtS
            | BinaryOp::I8x16GtU
            | BinaryOp::I8x16LeS
            | BinaryOp::I8x16LeU
            | BinaryOp::I8x16GeS
            | BinaryOp::I8x16GeU
            | BinaryOp::I16x8Eq
            | BinaryOp::I16x8Ne
            | BinaryOp::I16x8LtS
            | BinaryOp::I16x8LtU
            | BinaryOp::I16x8GtS
            | BinaryOp::I16x8GtU
            | BinaryOp::I16x8LeS
            | BinaryOp::I16x8LeU
            | BinaryOp::I16x8GeS
            | BinaryOp::I16x8GeU
            | BinaryOp::I32x4Eq
            | BinaryOp::I32x4Ne
            | BinaryOp::I32x4LtS
            | BinaryOp::I32x4LtU
            | BinaryOp::I32x4GtS
            | BinaryOp::I32x4GtU
            | BinaryOp::I32x4LeS
            | BinaryOp::I32x4LeU
            | BinaryOp::I32x4GeS
            | BinaryOp::I32x4GeU
            | BinaryOp::I64x2Eq
            | BinaryOp::I64x2Ne
            | BinaryOp::I64x2LtS
            | BinaryOp::I64x2GtS
            | BinaryOp::I64x2LeS
            | BinaryOp::I64x2GeS
            | BinaryOp::F32x4Eq
            | BinaryOp::F32x4Ne
            | BinaryOp::F32x4Lt
            | BinaryOp::F32x4Gt
            | BinaryOp::F32x4Le
            | BinaryOp::F32x4Ge
            | BinaryOp::F64x2Eq
            | BinaryOp::F64x2Ne
            | BinaryOp::F64x2Lt
            | BinaryOp::F64x2Gt
            | BinaryOp::F64x2Le
            | BinaryOp::F64x2Ge
            | BinaryOp::V128And
            | BinaryOp::V128Or
            | BinaryOp::V128Xor
            | BinaryOp::V128AndNot
            | BinaryOp::I8x16Shl
            | BinaryOp::I8x16ShrS
            | BinaryOp::I8x16ShrU
            | BinaryOp::I8x16Add
            | BinaryOp::I8x16AddSatS
            | BinaryOp::I8x16AddSatU
            | BinaryOp::I8x16Sub
            | BinaryOp::I8x16SubSatS
            | BinaryOp::I8x16SubSatU
            | BinaryOp::I16x8Shl
            | BinaryOp::I16x8ShrS
            | BinaryOp::I16x8ShrU
            | BinaryOp::I16x8Add
            | BinaryOp::I16x8AddSatS
            | BinaryOp::I16x8AddSatU
            | BinaryOp::I16x8Sub
            | BinaryOp::I16x8SubSatS
            | BinaryOp::I16x8SubSatU
            | BinaryOp::I16x8Mul
            | BinaryOp::I32x4Shl
            | BinaryOp::I32x4ShrS
            | BinaryOp::I32x4ShrU
            | BinaryOp::I32x4Add
            | BinaryOp::I32x4Sub
            | BinaryOp::I32x4Mul
            | BinaryOp::I64x2Shl
            | BinaryOp::I64x2ShrS
            | BinaryOp::I64x2ShrU
            | BinaryOp::I64x2Add
            | BinaryOp::I64x2Sub
            | BinaryOp::I64x2Mul
            | BinaryOp::F32x4Add
            | BinaryOp::F32x4Sub
            | BinaryOp::F32x4Mul
            | BinaryOp::F32x4Div
            | BinaryOp::F32x4Min
            | BinaryOp::F32x4Max
            | BinaryOp::F32x4PMin
            | BinaryOp::F32x4PMax
            | BinaryOp::F64x2Add
            | BinaryOp::F64x2Sub
            | BinaryOp::F64x2Mul
            | BinaryOp::F64x2Div
            | BinaryOp::F64x2Min
            | BinaryOp::F64x2Max
            | BinaryOp::F64x2PMin
            | BinaryOp::F64x2PMax
            | BinaryOp::I8x16NarrowI16x8S
            | BinaryOp::I8x16NarrowI16x8U
            | BinaryOp::I16x8NarrowI32x4S
            | BinaryOp::I16x8NarrowI32x4U
            | BinaryOp::I8x16RoundingAverageU
            | BinaryOp::I16x8RoundingAverageU
            | BinaryOp::I8x16MinS
            | BinaryOp::I8x16MinU
            | BinaryOp::I8x16MaxS
            | BinaryOp::I8x16MaxU
            | BinaryOp::I16x8MinS
            | BinaryOp::I16x8MinU
            | BinaryOp::I16x8MaxS
            | BinaryOp::I16x8MaxU
            | BinaryOp::I32x4MinS
            | BinaryOp::I32x4MinU
            | BinaryOp::I32x4MaxS
            | BinaryOp::I32x4MaxU
            | BinaryOp::I32x4DotI16x8S
            | BinaryOp::I16x8Q15MulrSatS
            | BinaryOp::I16x8ExtMulLowI8x16S
            | BinaryOp::I16x8ExtMulHighI8x16S
            | BinaryOp::I16x8ExtMulLowI8x16U
            | BinaryOp::I16x8ExtMulHighI8x16U
            | BinaryOp::I32x4ExtMulLowI16x8S
            | BinaryOp::I32x4ExtMulHighI16x8S
            | BinaryOp::I32x4ExtMulLowI16x8U
            | BinaryOp::I32x4ExtMulHighI16x8U
            | BinaryOp::I64x2ExtMulLowI32x4S
            | BinaryOp::I64x2ExtMulHighI32x4S
            | BinaryOp::I64x2ExtMulLowI32x4U
            | BinaryOp::I64x2ExtMulHighI32x4U => unimplemented!(),
        }
    }

    fn generate_unop_fact(&self, pos: &str, instr: &Unop) {
        match instr.op {
            UnaryOp::I32Eqz => self.append_fact("Eqz", &fact!(pos)),
            UnaryOp::I32Clz => self.append_fact("Clz", &fact!(pos, "int")),
            UnaryOp::I32Ctz => self.append_fact("Ctz", &fact!(pos, "int")),
            UnaryOp::I32Popcnt => self.append_fact("Popcnt", &fact!(pos, "int")),
            UnaryOp::I64Eqz => self.append_fact("Eqz", &fact!(pos)),
            UnaryOp::I64Clz => self.append_fact("Clz", &fact!(pos, "long long")),
            UnaryOp::I64Ctz => self.append_fact("Ctz", &fact!(pos, "long long")),
            UnaryOp::I64Popcnt => self.append_fact("Popcnt", &fact!(pos, "long long")),
            UnaryOp::F32Abs => self.append_fact("Abs", &fact!(pos, "float")),
            UnaryOp::F32Neg => self.append_fact("Neg", &fact!(pos, "float")),
            UnaryOp::F32Ceil => self.append_fact("Ceil", &fact!(pos, "float")),
            UnaryOp::F32Floor => self.append_fact("Floor", &fact!(pos, "float")),
            UnaryOp::F32Trunc => self.append_fact("FTrunc", &fact!(pos, "float")),
            UnaryOp::F32Nearest => self.append_fact("Nearest", &fact!(pos, "float")),
            UnaryOp::F32Sqrt => self.append_fact("Sqrt", &fact!(pos, "float")),
            UnaryOp::F64Abs => self.append_fact("Abs", &fact!(pos, "double")),
            UnaryOp::F64Neg => self.append_fact("Neg", &fact!(pos, "double")),
            UnaryOp::F64Ceil => self.append_fact("Ceil", &fact!(pos, "double")),
            UnaryOp::F64Floor => self.append_fact("Floor", &fact!(pos, "double")),
            UnaryOp::F64Trunc => self.append_fact("FTrunc", &fact!(pos, "double")),
            UnaryOp::F64Nearest => self.append_fact("Nearest", &fact!(pos, "double")),
            UnaryOp::F64Sqrt => self.append_fact("Sqrt", &fact!(pos, "double")),
            UnaryOp::I32WrapI64 => self.append_fact("Wrap", &fact!(pos)),
            UnaryOp::I32TruncSF32 => self.append_fact("ITrunc", &fact!(pos, "int", "float", "$s")),
            UnaryOp::I32TruncUF32 => self.append_fact("ITrunc", &fact!(pos, "int", "float", "$u")),
            UnaryOp::I32TruncSF64 => self.append_fact("ITrunc", &fact!(pos, "int", "double", "$s")),
            UnaryOp::I32TruncUF64 => self.append_fact("ITrunc", &fact!(pos, "int", "double", "$u")),
            UnaryOp::I64ExtendSI32 => self.append_fact("ExtendI32", &fact!(pos, "$s")),
            UnaryOp::I64ExtendUI32 => self.append_fact("ExtendI32", &fact!(pos, "$u")),
            UnaryOp::I64TruncSF32 => {
                self.append_fact("ITrunc", &fact!(pos, "long long", "float", "$s"))
            }
            UnaryOp::I64TruncUF32 => {
                self.append_fact("ITrunc", &fact!(pos, "long long", "float", "$u"))
            }
            UnaryOp::I64TruncSF64 => {
                self.append_fact("ITrunc", &fact!(pos, "long long", "double", "$s"))
            }
            UnaryOp::I64TruncUF64 => {
                self.append_fact("ITrunc", &fact!(pos, "long long", "double", "$u"))
            }
            UnaryOp::F32ConvertSI32 => {
                self.append_fact("Convert", &fact!(pos, "float", "int", "$s"))
            }
            UnaryOp::F32ConvertUI32 => {
                self.append_fact("Convert", &fact!(pos, "float", "int", "$u"))
            }
            UnaryOp::F32ConvertSI64 => {
                self.append_fact("Convert", &fact!(pos, "float", "long long", "$s"))
            }
            UnaryOp::F32ConvertUI64 => {
                self.append_fact("Convert", &fact!(pos, "float", "long long", "$u"))
            }
            UnaryOp::F32DemoteF64 => self.append_fact("Demote", &fact!(pos)),
            UnaryOp::F64ConvertSI32 => {
                self.append_fact("Convert", &fact!(pos, "double", "int", "$s"))
            }
            UnaryOp::F64ConvertUI32 => {
                self.append_fact("Convert", &fact!(pos, "double", "int", "$u"))
            }
            UnaryOp::F64ConvertSI64 => {
                self.append_fact("Convert", &fact!(pos, "double", "long long", "$s"))
            }
            UnaryOp::F64ConvertUI64 => {
                self.append_fact("Convert", &fact!(pos, "double", "long long", "$u"))
            }
            UnaryOp::F64PromoteF32 => self.append_fact("Promote", &fact!(pos)),
            UnaryOp::I32ReinterpretF32 => {
                self.append_fact("Reinterpret", &fact!(pos, "int", "float"))
            }
            UnaryOp::I64ReinterpretF64 => {
                self.append_fact("Reinterpret", &fact!(pos, "long long", "double"))
            }
            UnaryOp::F32ReinterpretI32 => {
                self.append_fact("Reinterpret", &fact!(pos, "float", "int"))
            }
            UnaryOp::F64ReinterpretI64 => {
                self.append_fact("Reinterpret", &fact!(pos, "double", "long long"))
            }
            UnaryOp::I32Extend8S => self.append_fact("Extend8", &fact!(pos, "int")),
            UnaryOp::I32Extend16S => self.append_fact("Extend16", &fact!(pos, "int")),
            UnaryOp::I64Extend8S => self.append_fact("Extend8", &fact!(pos, "long long")),
            UnaryOp::I64Extend16S => self.append_fact("Extend16", &fact!(pos, "long long")),
            UnaryOp::I64Extend32S => self.append_fact("Extend32", &fact!(pos, "long long")),
            UnaryOp::I8x16Splat
            | UnaryOp::I8x16ExtractLaneS { idx: _ }
            | UnaryOp::I8x16ExtractLaneU { idx: _ }
            | UnaryOp::I16x8Splat
            | UnaryOp::I16x8ExtractLaneS { idx: _ }
            | UnaryOp::I16x8ExtractLaneU { idx: _ }
            | UnaryOp::I32x4Splat
            | UnaryOp::I32x4ExtractLane { idx: _ }
            | UnaryOp::I64x2Splat
            | UnaryOp::I64x2ExtractLane { idx: _ }
            | UnaryOp::F32x4Splat
            | UnaryOp::F32x4ExtractLane { idx: _ }
            | UnaryOp::F64x2Splat
            | UnaryOp::F64x2ExtractLane { idx: _ }
            | UnaryOp::V128Not
            | UnaryOp::V128AnyTrue
            | UnaryOp::I8x16Abs
            | UnaryOp::I8x16Popcnt
            | UnaryOp::I8x16Neg
            | UnaryOp::I8x16AllTrue
            | UnaryOp::I8x16Bitmask
            | UnaryOp::I16x8Abs
            | UnaryOp::I16x8Neg
            | UnaryOp::I16x8AllTrue
            | UnaryOp::I16x8Bitmask
            | UnaryOp::I32x4Abs
            | UnaryOp::I32x4Neg
            | UnaryOp::I32x4AllTrue
            | UnaryOp::I32x4Bitmask
            | UnaryOp::I64x2Abs
            | UnaryOp::I64x2Neg
            | UnaryOp::I64x2AllTrue
            | UnaryOp::I64x2Bitmask
            | UnaryOp::F32x4Abs
            | UnaryOp::F32x4Neg
            | UnaryOp::F32x4Sqrt
            | UnaryOp::F32x4Ceil
            | UnaryOp::F32x4Floor
            | UnaryOp::F32x4Trunc
            | UnaryOp::F32x4Nearest
            | UnaryOp::F64x2Abs
            | UnaryOp::F64x2Neg
            | UnaryOp::F64x2Sqrt
            | UnaryOp::F64x2Ceil
            | UnaryOp::F64x2Floor
            | UnaryOp::F64x2Trunc
            | UnaryOp::F64x2Nearest
            | UnaryOp::I16x8ExtAddPairwiseI8x16S
            | UnaryOp::I16x8ExtAddPairwiseI8x16U
            | UnaryOp::I32x4ExtAddPairwiseI16x8S
            | UnaryOp::I32x4ExtAddPairwiseI16x8U
            | UnaryOp::I64x2ExtendLowI32x4S
            | UnaryOp::I64x2ExtendHighI32x4S
            | UnaryOp::I64x2ExtendLowI32x4U
            | UnaryOp::I64x2ExtendHighI32x4U
            | UnaryOp::I32x4TruncSatF64x2SZero
            | UnaryOp::I32x4TruncSatF64x2UZero
            | UnaryOp::F64x2ConvertLowI32x4S
            | UnaryOp::F64x2ConvertLowI32x4U
            | UnaryOp::F32x4DemoteF64x2Zero
            | UnaryOp::F64x2PromoteLowF32x4
            | UnaryOp::I32x4TruncSatF32x4S
            | UnaryOp::I32x4TruncSatF32x4U
            | UnaryOp::F32x4ConvertI32x4S
            | UnaryOp::F32x4ConvertI32x4U => unimplemented!(),
            UnaryOp::I32TruncSSatF32 => {
                self.append_fact("TruncSat", &fact!(pos, "int", "float", "$s"))
            }
            UnaryOp::I32TruncUSatF32 => {
                self.append_fact("TruncSat", &fact!(pos, "int", "float", "$u"))
            }
            UnaryOp::I32TruncSSatF64 => {
                self.append_fact("TruncSat", &fact!(pos, "int", "double", "$s"))
            }
            UnaryOp::I32TruncUSatF64 => {
                self.append_fact("TruncSat", &fact!(pos, "int", "double", "$u"))
            }
            UnaryOp::I64TruncSSatF32 => {
                self.append_fact("TruncSat", &fact!(pos, "long long", "float", "$s"))
            }
            UnaryOp::I64TruncUSatF32 => {
                self.append_fact("TruncSat", &fact!(pos, "long long", "float", "$u"))
            }
            UnaryOp::I64TruncSSatF64 => {
                self.append_fact("TruncSat", &fact!(pos, "long long", "double", "$s"))
            }
            UnaryOp::I64TruncUSatF64 => {
                self.append_fact("TruncSat", &fact!(pos, "long long", "double", "$u"))
            }
            UnaryOp::I16x8WidenLowI8x16S
            | UnaryOp::I16x8WidenLowI8x16U
            | UnaryOp::I16x8WidenHighI8x16S
            | UnaryOp::I16x8WidenHighI8x16U
            | UnaryOp::I32x4WidenLowI16x8S
            | UnaryOp::I32x4WidenLowI16x8U
            | UnaryOp::I32x4WidenHighI16x8S
            | UnaryOp::I32x4WidenHighI16x8U => unimplemented!(),
        }
    }

    fn generate_table_facts(&mut self) -> io::Result<()> {
        for (idx, table) in self.module.tables.iter().enumerate() {
            let min = table.initial;
            let max = table.maximum.unwrap_or(u32::MAX);
            let tpe = type_to_datalog(table.element_ty);
            self.append_fact("Table", &fact!(idx, min, max, tpe));
        }
        Ok(())
    }

    fn generate_memory_facts(&mut self) -> io::Result<()> {
        for memory in self.module.memories.iter() {
            let min = memory.initial;
            let max = memory.maximum.unwrap_or(u32::MAX);
            self.append_fact("Memory", &fact!(min, max));
        }
        Ok(())
    }

    fn generate_global_facts(&mut self) -> io::Result<()> {
        for (idx, global) in self.module.globals.iter().enumerate() {
            let mutable = mutable_to_datalog(global.mutable);
            let tpe = type_to_datalog(global.ty);
            let name = format!("global{idx}");
            self.append_fact("Global", &fact!(idx, mutable, tpe, name));
            if let GlobalKind::Local(init) = global.kind {
                let init = init_expr_to_datalog(init);
                self.append_fact("GlobalInit, ", &fact!(idx, init));
            }
        }
        Ok(())
    }

    fn generate_elem_facts(&mut self) -> io::Result<()> {
        for (idx, element) in self.module.elements.iter().enumerate() {
            let tpe = type_to_datalog(element.ty);
            let mode = element_kind_to_datalog(element.kind);
            self.append_fact("Elem", &fact!(idx, tpe, mode));
            for (init_idx, function) in element.members.iter().flatten().enumerate() {
                let init = format!("$func_ref({})", function.index());
                self.append_fact("ElemInits", &fact!(idx, init_idx, init));
            }
        }
        Ok(())
    }

    fn generate_data_facts(&mut self) -> io::Result<()> {
        for (idx, data) in self.module.data.iter().enumerate() {
            let mode = data_kind_to_datalog(data.kind);
            self.append_fact("Data", &fact!(idx, mode));
            for (init_idx, value) in data.value.iter().enumerate() {
                self.append_fact("DataInits", &fact!(idx, init_idx, value));
            }
        }
        Ok(())
    }
}

fn type_to_datalog(tpe: ValType) -> &'static str {
    match tpe {
        ValType::I32 => "int",
        ValType::I64 => "long long",
        ValType::F32 => "float",
        ValType::F64 => "double",
        ValType::V128 => unimplemented!(),
        ValType::Externref => "extern",
        ValType::Funcref => "func",
    }
}

fn mutable_to_datalog(mutable: bool) -> &'static str {
    match mutable {
        true => "$const",
        false => "$var",
    }
}

fn init_expr_to_datalog(init: InitExpr) -> String {
    match init {
        InitExpr::Value(value) => value_to_datalog_init_expr(value),
        InitExpr::Global(global) => format!("$global({})", global.index()),
        InitExpr::RefNull(tpe) => format!("$null_ref({})", type_to_datalog(tpe)),
        InitExpr::RefFunc(function) => format!("$func_ref({})", function.index()),
    }
}

fn value_to_datalog_init_expr(value: Value) -> String {
    match value {
        Value::I32(value) => format!("$i32_value({value})"),
        Value::I64(value) => format!("$i64_value({value})"),
        Value::F32(value) => format!("$f32_value({value})"),
        Value::F64(value) => format!("$f64_value({value})"),
        Value::V128(_) => unimplemented!(),
    }
}

fn element_kind_to_datalog(kind: ElementKind) -> String {
    match kind {
        ElementKind::Passive => "$elem_passive".to_string(),
        ElementKind::Declared => "$declarative".to_string(),
        ElementKind::Active { table, offset } => {
            format!(
                "$elem_active({},{})",
                table.index(),
                init_expr_to_datalog(offset)
            )
        }
    }
}

fn location_to_datalog(location: ActiveDataLocation) -> String {
    match location {
        ActiveDataLocation::Absolute(value) => format!("$i32_value({value})"),
        ActiveDataLocation::Relative(global) => format!("$global({})", global.index()),
    }
}

fn data_kind_to_datalog(kind: DataKind) -> String {
    match kind {
        DataKind::Passive => "$data_passive".to_string(),
        DataKind::Active(data) => {
            format!("$data_active({})", location_to_datalog(data.location))
        }
    }
}

fn load_kind_to_datalog(kind: ExtendedLoad) -> &'static str {
    match kind {
        ExtendedLoad::SignExtend => "$s",
        ExtendedLoad::ZeroExtend => "$u",
        ExtendedLoad::ZeroExtendAtomic => unimplemented!(),
    }
}
