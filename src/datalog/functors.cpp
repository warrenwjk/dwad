#include <string>

#include "souffle/SouffleInterface.h"

using namespace std;

extern "C" {
int32_t type_prio(souffle::SymbolTable* symbolTable,
                  souffle::RecordTable* recordTable, souffle::RamDomain type) {
  assert(symbolTable && "NULL symbol table");
  assert(recordTable && "NULL record table");
  const std::string& stype = symbolTable->decode(type);
  int32_t idx = stype.length();
  while (stype[--idx] == '*')
    ;
  const std::string base_type = stype.substr(0, idx + 1);
  if (base_type.compare("float") == 0)
    return 10;
  else if (base_type.compare("double") == 0)
    return 9;
  else if (base_type.compare("unsigned char") == 0)
    return 8;
  else if (base_type.compare("char") == 0)
    return 7;
  else if (base_type.compare("unsigned short") == 0)
    return 6;
  else if (base_type.compare("short") == 0)
    return 5;
  else if (base_type.compare("unsigned") == 0)
    return 4;
  else if (base_type.compare("unsigned long long") == 0)
    return 3;
  else if (base_type.compare("long long") == 0)
    return 2;
  else if (base_type.compare("int") == 0)
    return 1;
  return 0;
}

int32_t pointer_depth(souffle::SymbolTable* symbolTable,
                      souffle::RecordTable* recordTable,
                      souffle::RamDomain type) {
  assert(symbolTable && "NULL symbol table");
  assert(recordTable && "NULL record table");
  const std::string& stype = symbolTable->decode(type);
  int32_t idx = stype.length();
  while (stype[--idx] == '*')
    ;
  return stype.length() - (idx + 1);
}

souffle::RamDomain binop(souffle::SymbolTable* symbolTable,
                         souffle::RecordTable* recordTable,
                         souffle::RamDomain lhs, souffle::RamDomain rhs,
                         souffle::RamDomain op) {
  assert(symbolTable && "NULL symbol table");
  assert(recordTable && "NULL record table");
  const std::string& slhs = symbolTable->decode(lhs);
  const std::string& srhs = symbolTable->decode(rhs);
  const std::string& sop = symbolTable->decode(op);
  std::string result = "(" + slhs + sop + srhs + ")";
  return symbolTable->encode(result);
}

souffle::RamDomain infix(souffle::SymbolTable* symbolTable,
                         souffle::RecordTable* recordTable,
                         souffle::RamDomain lhs, souffle::RamDomain rhs,
                         souffle::RamDomain op) {
  assert(symbolTable && "NULL symbol table");
  assert(recordTable && "NULL record table");
  const std::string& slhs = symbolTable->decode(lhs);
  const std::string& srhs = symbolTable->decode(rhs);
  const std::string& sop = symbolTable->decode(op);
  std::string result = slhs + sop + srhs;
  return symbolTable->encode(result);
}

souffle::RamDomain type_code(souffle::SymbolTable* symbolTable,
                             souffle::RecordTable* recordTable,
                             souffle::RamDomain type) {
  assert(symbolTable && "NULL symbol table");
  assert(recordTable && "NULL record table");
  const std::string& stype = symbolTable->decode(type);
  int32_t idx = stype.length();
  while (stype[--idx] == '*')
    ;
  const std::string base_type = stype.substr(0, idx + 1);
  if (base_type.compare("float") == 0)
    return symbolTable->encode("f" + to_string(stype.length() - (idx + 1)));
  else if (base_type.compare("double") == 0)
    return symbolTable->encode("d" + to_string(stype.length() - (idx + 1)));
  else if (base_type.compare("unsigned char") == 0)
    return symbolTable->encode("uc" + to_string(stype.length() - (idx + 1)));
  else if (base_type.compare("char") == 0)
    return symbolTable->encode("c" + to_string(stype.length() - (idx + 1)));
  else if (base_type.compare("unsigned short") == 0)
    return symbolTable->encode("us" + to_string(stype.length() - (idx + 1)));
  else if (base_type.compare("short") == 0)
    return symbolTable->encode("s" + to_string(stype.length() - (idx + 1)));
  else if (base_type.compare("unsigned") == 0)
    return symbolTable->encode("u" + to_string(stype.length() - (idx + 1)));
  else if (base_type.compare("unsigned long long") == 0)
    return symbolTable->encode("ull" + to_string(stype.length() - (idx + 1)));
  else if (base_type.compare("long long") == 0)
    return symbolTable->encode("ll" + to_string(stype.length() - (idx + 1)));
  else if (base_type.compare("int") == 0)
    return symbolTable->encode("i" + to_string(stype.length() - (idx + 1)));
  return symbolTable->encode(to_string(stype.length() - (idx + 1)));
}

souffle::RamDomain trim_parens(souffle::SymbolTable* symbolTable,
                               souffle::RecordTable* recordTable,
                               souffle::RamDomain sym) {
  assert(symbolTable && "NULL symbol table");
  assert(recordTable && "NULL record table");
  const std::string& ssym = symbolTable->decode(sym);
  if (ssym.substr(0, 1).compare("(") == 0) {
    return symbolTable->encode(ssym.substr(1, ssym.length() - 2));
  } else {
    return symbolTable->encode(ssym);
  }
}

souffle::RamDomain cat_semi(souffle::SymbolTable* symbolTable,
                            souffle::RecordTable* recordTable,
                            souffle::RamDomain sym) {
  assert(symbolTable && "NULL symbol table");
  assert(recordTable && "NULL record table");
  const std::string& ssym = symbolTable->decode(sym);
  if (ssym.substr(ssym.length() - 1, 1).compare(";") == 0) {
    return symbolTable->encode(ssym);
  } else {
    return symbolTable->encode(ssym + ";");
  }
}
}
